#!/bin/sh

set -e

echo "Detecting languages..."

cd /usr/share/nginx/html

LANGUAGES="$(find . -type d -maxdepth 1 | grep "./" | sed 's/^\.\///' | tr '\n' '|' | sed 's/|$//')"

echo "Detected languages: $LANGUAGES"

cd /

export LANGUAGES

# should exit if the list of languages is empty
if [ -z "$LANGUAGES" ]; then
    echo "No languages detected"
    exit 1
fi

# use existing default or first language from LANGUAGES
DEFAULT_LANGUAGE=${DEFAULT_LANGUAGE:-$(echo "$LANGUAGES" | cut -d"|" -f1)}

# check if the default language is in the list of languages
if ! echo "$LANGUAGES" | grep -q "${DEFAULT_LANGUAGE}"; then
    echo "Default language '$DEFAULT_LANGUAGE' not found in LANGUAGES"
    echo "Setting default language to first language found in LANGUAGES"
    DEFAULT_LANGUAGE=$(echo "$LANGUAGES" | cut -d"|" -f1)
fi

echo "Default language: $DEFAULT_LANGUAGE"

# should exit if the default language is empty
if [ -z "$DEFAULT_LANGUAGE" ]; then
    echo "Default language is empty"
    exit 1
fi

echo "map \$http_accept_language \$accept_language {" > /etc/nginx/conf.d/accept-language.conf
echo "    default ${DEFAULT_LANGUAGE};" >> /etc/nginx/conf.d/accept-language.conf
# loop through all languages
for lang in $(echo "$LANGUAGES" | tr "|" "\n")
do
    echo "    ~*^${lang} ${lang};" >> /etc/nginx/conf.d/accept-language.conf
done
echo "}" >> /etc/nginx/conf.d/accept-language.conf


echo "Starting nginx with ..."

# if $@ is empty, start nginx
if [ -z "$@" ]; then
  /docker-entrypoint.sh "nginx" "-g" "daemon off;"
else
  /docker-entrypoint.sh "$@"
fi
