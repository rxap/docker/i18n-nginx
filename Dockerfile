ARG NGINX_TAG=alpine

FROM nginx:${NGINX_TAG}

RUN mkdir -p /utility

COPY start.sh /utility/start.sh

RUN rm -fr /usr/share/nginx/html/*

RUN apk --no-cache add curl

COPY accept-language.conf /etc/nginx/conf.d/accept-language.conf

COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf.template /etc/nginx/templates/default.conf.template

COPY scheme_logic.conf /etc/nginx/scheme_logic.conf

HEALTHCHECK --interval=30s --timeout=5s --start-period=5s --retries=3 \
    CMD curl --fail --location http://localhost:80/index.html || exit 1

ENTRYPOINT ["/utility/start.sh"]
CMD ["nginx", "-g", "daemon off;"]
