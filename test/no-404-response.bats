#!/usr/bin/env bats

load ./load.bash

@test "should always return the index.html of the language root directory for the default language if not preferred language is set" {
  run http --print=b --follow GET "$baseUrl"
  assert_success
  assert_output --partial "<base href=\"/en/\"/>"
}

@test "should always return the index.html of the language root directory for the preferred language" {
  run http --print=b --follow GET "$baseUrl" "Accept-Language: de"
  assert_success
  assert_output --partial "<base href=\"/de/\"/>"
}

@test "should always return the index.html of the language root directory for the default language if not preferred language is set and only the root path is used" {
  run http --print=b --follow GET "$baseUrl/"
  assert_success
  assert_output --partial "<base href=\"/en/\"/>"
}

@test "should always return the index.html of the language root directory for the preferred language and only the root path is used" {
  run http --print=b --follow GET "$baseUrl/" "Accept-Language: de"
  assert_success
  assert_output --partial "<base href=\"/de/\"/>"
}

@test "should always return the index.html of the language root directory for the default language if not preferred language is set independent of the path" {
  run http --print=b --follow GET "$baseUrl/custom/sub/path"
  assert_success
  assert_output --partial "<base href=\"/en/\"/>"
}

@test "should always return the index.html of the language root directory for the preferred language independent of the path" {
  run http --print=b --follow GET "$baseUrl/custom/sub/path" "Accept-Language: de"
  assert_success
  assert_output --partial "<base href=\"/de/\"/>"
}

@test "should always return the index.html of the default language if the given language is not supported" {
  run http --print=b --follow GET "$baseUrl/xx/custom/sub/path"
  assert_success
  assert_output --partial "<base href=\"/en/\"/>"
}
