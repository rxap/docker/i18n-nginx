#!/usr/bin/env bats

load ./load.bash

@test "redirect to default language if no preferred language is requested and no path is given" {
  run http --print=h GET "$baseUrl"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/en"
}

@test "redirect to default language if no preferred language is requested and only to root path is given" {
  run http --print=h GET "$baseUrl/"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/en/"
}

@test "redirect to requested preferred language if preferred language is requested and no path is given" {
  run http --print=h GET "$baseUrl/" "Accept-Language: de"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/de/"
}

@test "redirect to requested preferred language if preferred language is requested and only to root path is given" {
  run http --print=h GET "$baseUrl" "Accept-Language: de"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/de"
}

@test "redirect to default language if preferred language is not available" {
  run http --print=h GET "$baseUrl" "Accept-Language: er"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/en"
}

@test "redirect to default language if selected language is not available" {
  run http --print=h GET "$baseUrl/er" "Accept-Language: er"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/en"
}

@test "redirect to default language while preserving the given path if no preferred language" {
  run http --print=h GET "$baseUrl/custom/path"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/en/custom/path"
}

@test "redirect to requested preferred language while preserving the given path if preferred language" {
  run http --print=h GET "$baseUrl/custom/path" "Accept-Language: de"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/de/custom/path"
}

@test "redirect to default language while preserving the given path and args if no preferred language" {
  run http --print=h GET "$baseUrl/custom/path?foo=bar"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/en/custom/path?foo=bar"
}

@test "redirect to requested preferred language while preserving the given path and args if preferred language" {
  run http --print=h GET "$baseUrl/custom/path?foo=bar" "Accept-Language: de"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  assert_output --partial "Location: $baseUrl/de/custom/path?foo=bar"
}

@test "redirect should preserver the requesting protocol" {
  run http --print=h GET "$baseUrl/custom/path" "X-Forwarded-Proto: https"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  if [ "${ROOT_DOMAIN_PORT:-9999}" -eq "80" ]; then
    assert_output --partial "Location: https://$ROOT_DOMAIN/en/custom/path"
  else
    assert_output --partial "Location: https://$ROOT_DOMAIN:${ROOT_DOMAIN_PORT:-9999}/en/custom/path"
  fi
}

@test "redirect should preserver the requesting protocol if preferred language is requested" {
  run http --print=h GET "$baseUrl/custom/path" "X-Forwarded-Proto: https" "Accept-Language: de"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  if [ "${ROOT_DOMAIN_PORT:-9999}" -eq "80" ]; then
    assert_output --partial "Location: https://$ROOT_DOMAIN/de/custom/path"
  else
    assert_output --partial "Location: https://$ROOT_DOMAIN:${ROOT_DOMAIN_PORT:-9999}/de/custom/path"
  fi
}

@test "redirect should preserver the requesting protocol if the language path is already present" {
  run http --print=h GET "$baseUrl/de/custom/path" "X-Forwarded-Proto: https"
  assert_success
  assert_output --partial "HTTP/1.1 200 OK"
}

@test "redirect should preserver the requesting protocol if the language path is already present but with a trailing slash" {
  run http --print=h GET "$baseUrl/de" "X-Forwarded-Proto: https"
  assert_success
  assert_output --partial "HTTP/1.1 302 Moved Temporarily"
  if [ "${ROOT_DOMAIN_PORT:-9999}" -eq "80" ]; then
    assert_output --partial "Location: https://$ROOT_DOMAIN/de/"
  else
    assert_output --partial "Location: https://$ROOT_DOMAIN:${ROOT_DOMAIN_PORT:-9999}/de/"
  fi
}
